

% load existing parameters from file into variable 'params'
    load('base_params.mat','params');
    
% change a parameter
    params.psi = 0.2;
    
% add a new parameter
    params.new = 5;
   
% save parameter file
    save('new_params.mat','params');
    
 
