%Exploring the evolution of strains. This script study a case where the
%infection spreads according to its force, there's a gono_source and the
%non-AMR can mutate upon treatment.
clear;clc;
% General parmeters 
N = 3e3;          % population size
n_Days = 365;     % days to simulate
VERBOSE = false;
LOW_MEM = true;
% load preset simulation parameters from external file
% TEST 3
load('base_params.mat','params');  
params.p0=[0.1 0 0]; %
params.ALLOW_DRES=true;
params.ALLOW_COINFECTION=false;%Added by NVV% no coinfection will be considered
params.dres_rate=0.0; %turn off for now
params.ALLOW_TREAT=true;
params.P_BLINDTREAT_AS_AMR=0;
VERBOSE=true;
params.BETA=2e-4*[1 1];
params.R=6e-4;
params.eta=3e-4; %new property to control the source of gonorrhoea

params.EFF_TREAT=false;

%simulation
inicial=20;
tic
gono_model = AMR_IBM(N, params, [], VERBOSE, LOW_MEM);
gono_model.simulate(inicial*n_Days);
toc
gono_model.ALLOW_DRES=true;
%gono_model.dres_rate=5e-2;
gono_model.dres_rate=2e-2;
tic
gono_model.simulate(1.5*inicial*n_Days);
toc;

%gono.model.EFF_TREAT=true;
%gono.model.simulate(inicial*n_Days);
% 
%   figure(1)
% %  subplot(2,1,1)
% %  
%  plot([0:gono_model.today]/365,(gono_model.counters.prevalence-gono_model.counters.trem),'--');
%  xlabel('time[years]'); ylabel('% infected');legend('non-AMR', 'AMR','Location','NorthWest');title('infected outside of the LCC');
%  text(inicial,0,'mutation begins','rotation',90);grid on; 
% % % 
%  subplot(2,1,2)
plot([0:gono_model.today-1]/365,gono_model.counters.trem(1:end-1,:)/N*100,'--'); hold on;
xlabel('time[years]'); ylabel('% of infected in the L.C.C.');legend('non-AMR', 'AMR','Location','NorthWest');title('largest connected component');
text(inicial,5,'mutation begins','rotation',90);grid on; 

% figure(2); 
% gono_model.plot_net_graph(gono_model, gono_model.adj_full)

 figure(2); 
plot([0:gono_model.today]/365,gono_model.counters.prevalence(:,2)./(gono_model.counters.prevalence(:,1)+gono_model.counters.prevalence(:,2))*100,'r');
legend('AMR');xlabel('time[years]'); ylabel('% of infections of this type');
text(inicial,5,'mutation begins','rotation',90);grid on; 


%38900
