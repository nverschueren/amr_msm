%Exploring the evolution of strains
clear;clc;

    % General parmeters 
             N = 5e3;          % population size
            n_Days =  3*365;     % days to simulate

            VERBOSE = true;
            %        LOW_MEM = false;
                   LOW_MEM = true;
                   

        % load preset simulation parameters from external file
            load('base_params.mat','params');  
            params.p0(2)=0;
            %assigning new properties' values
            %params.ALLOW_DRES=false;
            params.ALLOW_DRES=true;
            params.ALLOW_COINFECTION=false;% no coinfection will be considered
            params.dres_rate=0.5; %brutal, just for testing
            
            %for testing purposes only
            params.ALLOW_TREAT=true;
            VERBOSE=false;
            %params.MU=0;
            %params.R=0;
            %params.BETA=0;
                       
            
            gono_model = AMR_IBM(N, params, [], VERBOSE, LOW_MEM);
            gono_model.simulate(1);
            
            