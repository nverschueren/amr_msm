
%% Try to fill the MSM network in a different way...
% This is a good idea, but needs to be polish a bit
clear; close all; clc;
N=1e4;
d_min=1; 
alpha=1.6;
d_max=120;
ndm =fix( ( ( d_max^(-alpha+1) - d_min^(-alpha+1) ).*rand(N,1) + d_min^(-alpha+1) ).^( 1/(-alpha+1) ));
auxdm=ndm;
amm=logical(sparse(N,N));
notincr=0;
sumold=-1;
tic;
sums=[];
while notincr<50
    
    
    
    avid=find(auxdm>0);%find the people with availability
    
    %divide the population with availability into two with the same
    %number of individuals, each of size M
    if mod(length(avid),2)~=0  
        M=(length(avid)-1)/2; 
    else
        M=length(avid)/2; 
    end
    
    ind=randperm(length(avid),2*M); %shuffle them
    
    %[length(ind(1:M))  length(ind(M+1:end))]
    
    
    fh=avid(ind(1:M)); %first half
    sh=avid(ind(M+1:end));%second half
    
    
    for i=1:M
        if ~amm(fh(i),sh(i))
            amm(fh(i),sh(i))=1; 
            amm(sh(i),fh(i))=1; 
            auxdm(sh(i))=auxdm(sh(i))-1;
            auxdm(fh(i))=auxdm(fh(i))-1;
        end
    end
    
    sums=[sums,sum(auxdm)];
    if sumold==sum(auxdm)
        notincr=notincr+1;
    end
            sumold=sum(auxdm);
        
sums=[sums,sumold];
    
    
end
toc



[ sum(auxdm)  sum(ndm)]
sum(auxdm)/sum(ndm)
%THis needs to be fixed more carefully. Once this is ready, the new
%function will be faster and better than the original one. As a
%final thing, we could add a last step which runs the function
%either 10 times or until the number of loose connections is less
%than the 0.001%

disp('let us do more!')
for i=1:length(avid)
    if ~prod(full(amm(avid(i),avid(i+1:end))))
       indix=find(amm(avid(i),avid(i+1:end))==0);
       for j=1:indix
          amm(indix(i),indix(j))=1;
          amm(indix(j),indix(i))=1;
          auxdm(indix(i))=auxdm(indix(i))-1;
          auxdm(indix(j))=auxdm(indix(j))-1;
          j
          disp('more connections established!')
       end
    end

    
    
end

[ sum(auxdm)  sum(ndm)]
sum(auxdm)/sum(ndm)