%Exploring the evolution of strains. This script study a case where the
%infection spreads according to its force, there's a gono_source and the
%non-AMR can mutate upon treatment.
clear;clc;
% General parmeters
N = 3e3;          % population size
n_Days = 365;     % days to simulate
VERBOSE = true;
LOW_MEM = true;
% load preset simulation parameters from external file
% TEST 3
iend=10;
for i=1:iend
    i
    load('base_params.mat','params');
    params.p0=[0.1 0 0]; %
    params.ALLOW_DRES=true;
    params.ALLOW_COINFECTION=false;%Added by NVV% no coinfection will be considered
    params.dres_rate=0.0; %turn off for now
    params.ALLOW_TREAT=true;    
    params.P_BLINDTREAT_AS_AMR=0;
    params.BETA=1.5e-3*[1 1];
    params.R=6e-3;%1.5e-3;
    params.eta=1.2e-3; %new property to control the source of gonorrhoea
    %2 %
    
    params.EFF_TREAT=false;
    
    %simulation
    inicial=10;
    %tic
    gono_model = AMR_IBM(N, params, [], VERBOSE, LOW_MEM);
    gono_model.simulate(inicial*n_Days);
    %toc
    gono_model.ALLOW_DRES=true;
    gono_model.dres_rate=2e-3;
    %tic
    gono_model.simulate(70*n_Days);
    
    
 %   rr=[[0:gono_model.today]'/365,...
 %   gono_model.counters.prevalence/N*100,...
 %   gono_model.counters.prevalence(:,2)./(gono_model.counters.prevalence(:,1)+gono_model.counters.prevalence(:,2))*100];
    
    

   %save(sprintf('results/modelo%03d.mat',i),'rr')
    save(sprintf('results/modelo%03d.mat',i),'gono_model')
end

%descomentar para leer
N=3000;
a1=load('results/modelo001.mat');
prevman=a1.gono_model.counters.prevalence;
 for i=2:iend
     i
path=sprintf('results/modelo%.3i.mat',i);
modelo=load(path);
prevman=cat(3,prevman,modelo.gono_model.counters.prevalence); 


 end
 

mme=mean(prevman,3);
lower=mme-2*std(prevman,0,3)/sqrt(iend);
upper=mme+2*std(prevman,0,3)/sqrt(iend);
figure(1)

subplot(2,1,1)
plot([0:modelo.gono_model.today]/365,mme(:,1)/N*100)
grid on;
xlabel('time[years]'); ylabel('% prevalence'); title('MSM non-AMR')
hold on;
plot([0:modelo.gono_model.today]/365,lower(:,1)/N*100,'k');
hold on;
plot([0:modelo.gono_model.today]/365,upper(:,1)/N*100,'k');


subplot(2,1,2)
plot([0:modelo.gono_model.today]/365,mme(:,2)/N*100,'r')
grid on;
xlabel('time[years]'); ylabel('% prevalence'); title('MSM AMR')
hold on;
plot([0:modelo.gono_model.today]/365,lower(:,2)/N*100,'k');
hold on;
plot([0:modelo.gono_model.today]/365,upper(:,2)/N*100,'k');



razon1=prevman(:,2,:)./(prevman(:,1,:)+prevman(:,2,:));
razon6=prevman(:,2,:)./(6*prevman(:,1,:)+prevman(:,2,:));

razon1m=mean(razon1,3);
razon6m=mean(razon6,3);

razon1up=razon1m+2*std(razon1,0,3)/sqrt(iend);
razon1lo=razon1m-2*std(razon1,0,3)/sqrt(iend);

razon6up=razon6m+2*std(razon6,0,3)/sqrt(iend);
razon6lo=razon6m-2*std(razon6,0,3)/sqrt(iend);


figure(2)
subplot(2,1,1)
plot([0:modelo.gono_model.today]/365,razon1m*100);grid on
xlabel('time[years]'); ylabel('% of AMR cases'); title('amr/(non-amr*amr)')
hold on;
plot([0:modelo.gono_model.today]/365,razon1up*100,'k');
plot([0:modelo.gono_model.today]/365,razon1lo*100,'k');
subplot(2,1,2)
plot([0:modelo.gono_model.today]/365,razon6m*100);grid on
xlabel('time[years]'); ylabel('% of AMR cases'); title('amr/(6*non-amr*amr)')
hold on;
plot([0:modelo.gono_model.today]/365,razon6up*100,'k');
plot([0:modelo.gono_model.today]/365,razon6lo*100,'k');


razon1upf=floor(razon1up*100);
razon1lof=floor(razon1lo*100);

razon6upf=floor(razon6up*100);
razon6lof=floor(razon6lo*100);

figure(3)
plot([0:modelo.gono_model.today]/365,razon1upf,'.r'); hold on
plot([0:modelo.gono_model.today]/365,razon1lof,'.r'); hold on;
grid on
title('\theta_1')

figure(4)
plot([0:modelo.gono_model.today]/365,razon6upf,'.r'); hold on
plot([0:modelo.gono_model.today]/365,razon6lof,'.r'); hold on;
grid on;
title('\theta_6')



