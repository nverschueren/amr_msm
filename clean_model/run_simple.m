%% Simplified script to initialise, run and plot model output
% (single model run)

%    clear all;
%    clc;
%    close all;

    % General parmeters 
        N = 5e3;          % population size
        n_Days =  1825;     % days to simulate
    
        VERBOSE = true;
                LOW_MEM = false;
        %       LOW_MEM = true;
        
    % load preset simulation parameters from external file
        load('base_params.mat','params');
     
    % you can edit (baseline) parameters by overwriting the preset values, e.g.
        params.P_SYMPTOMS = 0.5;
        params.LAB_DELAY_MEAN = 12;
        % initial strain prevalence:
            % p0(1) = overall initial prevalence of gonorrhea (0.1 = 10%)
            % p0(2) = proportion of positive cases with AMR component
            % p0(3) = proportion of coinfection given AMR
            params.p0 = [0.2 0.1 0];
        
    % display all parameters
        params
        
    %% initialise model (create new model object)
        gono_model = AMR_IBM(N, params, [], VERBOSE, LOW_MEM);
     
        %       figure(1)=gono_model.plot_net_graph(gono_model,gono_model.adj_full);
        % [z]=gono_model.conncom(gono_model.adj_full);

        
        
        
        %% no treatment no birth/death or recovery
%         gono_mode.R=0;
%         gono_model.MU=0;
        %gono_model.ALLOW_TREAT=false;
    %% run simulation for n_Days # of days
    %n_Days = 365;
    %    gono_model.simulate(n_Days);
     
     %% extract all counter data from model object 
    % (or can be referenced directly)
    %        data = gono_model.counters
 
         % plot prevalence time-series for whole simulation
         % (using my built in function)
         %        gono_model.plot_prev(data, [0 n_Days], [])
         
         % manual plots (examples)
             % get prevalence (per strain) from counter variable
             % (not yet normalised with respect to the population size)
             %       prev_data = 100*data.prevalence./N
                 
         %    figure('name','Strain prevalence');
         %           hold on;
         %            plot([0:n_Days],prev_data(:,1),'b-'); % non AMR strain
         %           plot([0:n_Days],prev_data(:,2),'r-'); % AMR strain
         %           legend('non-AMR','AMR');
         %           xlabel('Time (days)')
         %           ylabel('Prevalence (%)');
         %           box on;
         %           grid on;
             
             % drug administration of each drug given by the cumulative sum
             % of the daily dosage of each drug
             %       figure('name','Dosage','color','w');
             %       hold on;
             %       plot([0:n_Days], cumsum(data.cipr),'b-');
             %       plot([0:n_Days], cumsum(data.cefta),'r-');
             %       legend('Cipr/A','Ceft/A','location','northwest');%
             %                   xlabel('Time (days)');
             %       ylabel('Number of doses')
             %       title('Cumulative drug doses administered');
             %       box on;
             %       grid on;
                     
     %% Change some parameters and continue simulation
     
         % at any point, we can change model parameters by directly
         % accessing the properties of the model object (this sort of thing
         % isn't usually adviced in object oriented programming - but we are
         % scientists, not developers!)
         
             % change diagnostics and treatment to discriminatory POC tests
             %   gono_model.ENABLE_POCT = true;
             %    gono_model.DISCRIM_POCT = true;
             
             % continue simulation for another n_Days
             %     gono_model.simulate(19*n_Days);
                 
             %% re-plot the cumulative dosages
             % this time, by accessing the data directly from the counter
             % variable in the model object. Can also get the last day of simluation
             % from the value in <object>.today
             %   figure('name','Dosage','color','w');
             %       hold on;
             %       plot([0:gono_model.today], cumsum(gono_model.counters.cipr),'b-');
             %       plot([0:gono_model.today], cumsum(gono_model.counters.cefta),'r-');
             %       ym = get(gca,'ylim');
             %       plot([n_Days n_Days],[ym(1) ym(2)],'k--');
             %       legend('Cipr/A','Ceft/A','location','northwest');
             %       text(n_Days-20, ym(2)/4,'pre-POCT (100% Ceft/A)','rotation',90,'color',[.4 .4 .4]);
             %       text(n_Days+15, ym(2)/3,'POCT introduced','rotation',90,'fontweight','bold');
             %       xlabel('Time (days)');
             %       ylabel('Number of doses')
             %       title('Cumulative drug doses administered');
             %       box on;
             %       grid on;
             

     %% extracting numerical data directly from model
     
     % Final prevalence of each strain (%): [nonAMR, AMR]
     % (note here we check the last (end) value of the 3rd dimension of the
     % state matrix - giving the last time entry)
     %prev = 100*sum(gono_model.indiv_state(:,:,end),1)./N
         
    