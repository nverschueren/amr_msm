%small example
clear;clc;
r=0.3;
a=zeros(100,2);
idx=randperm(length(a),length(a)*0.5);
a(idx,1)=1;

[sum(a(:,1)) sum(a(:,2)) ]
%^ is my initial situation

idx2=find(a(:,1));
 idx2prom=rand(length(idx2),1)<r;
 topromote=idx2(idx2prom);
 
 a(topromote,1)=0;
 a(topromote,2)=1;
 
 [sum(a(:,1)) sum(a(:,2)) ]
%now I want a 20% of the total number of 1 to be promoted to the second
%strain

    