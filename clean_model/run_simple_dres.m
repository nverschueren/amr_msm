%Exploring the evolution of strains
clear;clc;
% General parmeters
N = 5e3;          % population size
n_Days =  5*365;     % days to simulate. 5years
VERBOSE = true;
%        LOW_MEM = false;
LOW_MEM = true;


        % load preset simulation parameters from external file
            load('base_params.mat','params');  
            params.BETA=3e-3;
            %params.p0(2)=0;% zero AMR at the beginning 
            %assigning new properties' values
            params.ALLOW_DRES=false; %let us compare both cas
            %params.ALLOW_DRES=true;
            params.ALLOW_COINFECTION=false;% no coinfection will be considered
             params.dres_rate=1e-3; % 1/1000 cases of non-AMR will turn into AMR per day
           % params.dres_rate=0.5; % value for testing
            
            %for testing purposes only
            params.ALLOW_TREAT=true;
            VERBOSE=false;
          %  params.MU=0;
          %  params.R=0;
          %  params.BETA=0;
                       
            
            gono_model = AMR_IBM(N, params, [], VERBOSE, LOW_MEM);
            gono_model.simulate(n_Days);
            non_dres_data=gono_model.counters;
            
            %---- NOW, ALLOWING THE STRAIN TO DEVELOP RESISTANCE
            params.ALLOW_DRES=true; %let us compare both cas
            
            gono_model = AMR_IBM(N, params, [], VERBOSE, LOW_MEM);
            gono_model.simulate(n_Days);
            dres_data=gono_model.counters;
            
            %Now we can take a look at the differences in the counters, for
            %instance the prevalence in both cases
            
            subplot(2,1,1);
            plot([1:n_Days+1],non_dres_data.prevalence/N*100);
            xlabel('days'); ylabel('% of population')
            legend('non-AMR','AMR');
            title 'independent strains'
            subplot(2,1,2);
            plot([1:n_Days+1],dres_data.prevalence/N*100);
            xlabel('days'); ylabel('% of population')
            legend('non-AMR','AMR');
            title 'transference rate=1/1000'
            figure(2);
            plot([1:n_Days+1],dres_data.prev_either/N*100,'*r'); hold on
            plot([1:n_Days+1],non_dres_data.prev_either/N*100,'-k');legend('developing resistance','independent strains');
            xlabel('days');ylabel('% prevalence either strain')
            
                
            % a couple of things to consider.
            % THe treatment CAN be very different in the IBM, depending on
            % the regime considered and the strain (POCT, or not, which drugs, etc). 
            
            
            
            
            
            