%% Script to test run 2-strain AMR model
%
%
%  AUTHOR:  Adam Zienkiewicz, 2016 (adamziek@gmail.com)
%           University of Bristol
%
%function x = run_AMR_pregen()
    clear all;
    clc;
    close all;

    SHOW_PLOTS = true;

    %  DESCRIPTION of simulation (for saving output)
    DIR_NAME = 'results';


    % General parmeters ---------------------------------------------

    N = 1000;          % population size
    n_Days = 2*365;     % days to simulate
    n_Realiz = 4;       % number of model realisations to run and average
                         % (pre-generated networks if will be used if this
                         % is greater than 1, such that N has to
                         %  be 1000 or 10000 only!!)

    % randomise (fixed or shuffled?
    %rng(1);
    %rng('shuffle');

    % console output? (ON/OFF)
    VERBOSE = true;

    % save memory and store only current/final infection state of individuals
    LOW_MEM = true;

    %  extra debugging output? (ON/OFF)
    DEBUG = false;

    % Model parameters ------------------------------------------------
    params = struct;

        % initial strain prevalence:
            % p0(1) = overall initial prevalence of gonorrhea
            % p0(2) = proportion of positive cases with AMR component
            % p0(3) = proportion of coinfection given AMR
            %params.p0 = [0.07 0 0];
            %params.p0 = [0.15 0.5 0];
            params.p0 = [0.04 0.5 0.5];

        % start simulation with burn-in period (discarded) to obtain
        % desired AMR strain ratio? IGNORE
            params.burn_in = false;


        % partnership network (if not using pre-loaded networks )
            params.ALPHA = 1.6;                   % power-law degree distribution P(K) - k^-ALPHA 
            params.FULL_MAX_PARTNERS = 120;       % max. number of partners for any individual (yearly network)
            params.RESTRICT_MAX_PARTNERS = 10;    % max. number of partners in time-windowed period
            params.RESTRICT_RATE = 7;             % time-window of reduced partnership network (days)

        % DEFAULT infection / recovery pathway rate parameters
            params.BETA = [0.0022, 0.0022];         % transmission rates for nonAMR/AMR (/day) - for 10% with 10 partners (.2 /5 trace)

            params.R = 1/(21*7);                   % natural recovery rate (.day) (=1/mean disease lifetime)
            params.MU = 0.0000464;                 % birth / death rate (/day) [16 - 75 year olds]
            %params.MU = 0.0003;                    % birth / death rate (/day) [16 - 25 year olds] 

            params.GAMMA = 0.0025;                 % screening rate (/day) - 60% annual coverage = 0.0025
            %params.GAMMA = (1/90)*exp(-1/90);      % screening rate (/day) - ave. of once every three months (poisson)
            %params.GAMMA = (1/365)*exp(-1/365);      % screening rate (/day) - ave. of once per year (poisson)
            params.PSI = 0.1;                     % tracing efficiency 
            params.MAX_TRACE = 5;                 % max. number of partners who can be traced 

        % proportion of infecteds (either strain) who are symptomatic
            params.P_SYMPTOMS = 0.5;  
            %params.P_SYMPTOMS = 0.7;  

        % proportion of symptomatic individuals who will seek treatment
            params.P_SEEKS_TREATMENT = 0.66;
            %params.P_SEEKS_TREATMENT = 0.8;

        % proportion currently treated with recommended dual
        % therapy (Ceft/A) at point of care, without prior
        % knowledge of strain susceptibility
            params.P_BLINDTREAT_AS_AMR = 1;

        % time delays for lab results / symptom onset / seek /
        % recall and traces
            params.LAB_DELAY_MEAN = 10;  % value produced should be >= 1
            params.LAB_DELAY_STD = 1;
            params.ONSET_DELAY_MEAN = 5;
            params.ONSET_DELAY_STD = 1;
            params.SEEK_DELAY_MEAN = 10;
            params.SEEK_DELAY_STD = 2;
            params.RECALL_DELAY_MEAN = 7;
            params.RECALL_DELAY_STD = 0.5;
            params.TRACE_DELAY_MEAN = 7;
            params.TRACE_DELAY_STD = 1;

        % enable new routes for nonAMR treatment using information
        % available from RECALL / TRACE
        % (nonAMR treatment can still occur if P_BLINDTREAT_AS_AMR < 1)

        % when this set to true - AMR flag for recalls can be used to treat
        % index patients as nonAMR when AMR flag is zero
            params.ENABLE_nonAMR_RECALL = false;

        % when this set to true - AMR flag for traced indivs can be used to treat
        % secondary patients as nonAMR when AMR flag is zero
            params.ENABLE_nonAMR_TRACE = false;

        % old variable (not used) - for backwards compatibility
            params.ENABLE_nonAMR_CARE = false;

        % enable point-of-care test for all treatment seeking routes
        % (will override other options where appropriate)
            params.ENABLE_POCT = false;
            params.DISCRIM_POCT = true;

        % toggle whether traced individuals are screened rather than treated
        % immediately
            params.PRESCREEN_TRACED = false;

        % toggle whether voluntarily seeking individuals are screened
        % rather than treated immediately 
            params.PRESCREEN_SEEKED = false;

        % maximum delay between begin flagged as nonAMR (recall/trace) and being treated as nonAMR
            params.NON_AMR_MAX_DELAY = inf;

        % maximum # parnters for a recalled/traced individual to be treated as nonAMR 
            params.NON_AMR_MAX_PARTNERS = inf;


        % if ALLOW_COINFECTION = true then individuals can be infected with1
        % both nonAMR and AMR strains simulteneously.
        % if false, then an individual already infected with one strain,
        % cannot contract the other
            params.ALLOW_COINFECTION = true;

        % TOGGLE TREATMENT - ON/OFF  (OFF will prevent any antibiotic
        % treatment)
            params.ALLOW_TREAT = true;

        % parameter updates
            param_updates = {};
            % % other parameters which will be applied after equilibration (optional)
            % eq_days = 1000;
            % param_updates = {'self.PRESCREEN_TRACED = true',eq_days;...
            %                  'self.PRESCREEN_SEEKED = true',eq_days;...
            %                  'self.ENABLE_nonAMR_CARE = true', eq_days;...
            %                  'self.LAB_DELAY_MEAN = 1', eq_days};

        % save params
            if exist('./outdata','dir')~=7;mkdir('./outdata');end
            if exist(['./outdata/',DIR_NAME],'dir')~=7;mkdir(['./outdata/',DIR_NAME]);end
            save(['./outdata/',DIR_NAME,'/params.mat'],'params','N');
            diary(['./outdata/',DIR_NAME,'/params.txt'])
            display(params)
            diary off

        % init adjacency matrix
            adj = [];
            bin_edges = [0 1 2 5 13 30 params.FULL_MAX_PARTNERS+1];

%---------------------------------------------------------------------    

    
    if n_Realiz <= 1
        % Initialise SINGLE model realisation (class state will be saved)

            % Start with empty network data 
            % (force model to generate new random network)
                adj_set = [];

            % initialise model class
                tic;
                ibm = AMR_IBM(N, params, adj_set, VERBOSE, LOW_MEM);
                ibm.DEBUG = DEBUG;
                ibm.DIR_NAME = DIR_NAME;
                ibm.param_updates = param_updates;
                
                % simulate model for number of days specified
                    ibm.simulate(n_Days);
                    toc;
                % extract all counter data from model class 
                    data = ibm.counters;
                    data.N = N;
                    
    else % (n_Realiz > 1)
        % Run a bunch of simulations in parallel and average their results
        %
        %  On most modern CPUs this will enable 4 concurrent (parallel) simulations
        %  - on Bluecrystal we can use high memory nodes and up to 12 workers!
        
        % disable output on screen
            VERBOSE = false;

        % save memory and store only current/final infection state of individuals
            LOW_MEM = true;

        % disable extra debugging output
            DEBUG = false;
    
        % accumulation arrays 
        % (extract data from all simulations which can be averaged later
            av_prevalence = zeros(n_Days+1, 2, n_Realiz);
            av_prev_both = zeros(n_Days+1,1, n_Realiz);
            av_prev_either = zeros(n_Days+1,1, n_Realiz);
            av_prev_nonAMRonly = zeros(n_Days+1,1, n_Realiz);
            av_incidence = zeros(n_Days+1, 2, n_Realiz);
            av_incidence_either = zeros(n_Days+1,1, n_Realiz);
            av_incidence_new = zeros(n_Days+1,1, n_Realiz);
            av_cefta = zeros(n_Days+1,1, n_Realiz);
            av_cefta_notinf = zeros(n_Days+1,1, n_Realiz);
            av_cefta_nonAMR = zeros(n_Days+1,1, n_Realiz);
            av_cefta_AMR = zeros(n_Days+1,1, n_Realiz);
            av_cipr = zeros(n_Days+1,1, n_Realiz);
            av_cipr_notinf = zeros(n_Days+1,1, n_Realiz);
            av_cipr_nonAMR = zeros(n_Days+1,1, n_Realiz);
            av_cipr_AMR = zeros(n_Days+1,1, n_Realiz);
            av_treatpositive = zeros(n_Days+1,1, n_Realiz);
            av_n_attended_seeked = zeros(n_Days+1,3, n_Realiz);
            av_n_attended_recalled = zeros(n_Days+1,3, n_Realiz);
            av_n_attended_traced = zeros(n_Days+1,3, n_Realiz);
            av_n_attended_treated = zeros(n_Days+1,3, n_Realiz);
            av_n_treated_infected_symptomatic = zeros(n_Days+1,2, n_Realiz);
            av_n_screened = zeros(n_Days+1,3, n_Realiz);
            av_n_traced = zeros(n_Days+1,3, n_Realiz);
            av_int_dseq_all = zeros(length(bin_edges),1,n_Realiz);
            av_int_dseq_ofinfected = zeros(length(bin_edges),1,n_Realiz);
        
        % load required number of networks from file
            fprintf('Reading networks from file...');
            tic;
            net_file = matfile(['networks_N=',num2str(N),'_alpha=1.6_dmax=120(struct).mat'],'writable',false);

            % annoyingly can only read consequetive networks (CARE!) held
            % in file - so chose random start number  
            s = randi([1,size(net_file.adj_set,2)-n_Realiz],1);
            adj_set = net_file.adj_set(1,s:s+(n_Realiz-1));
            toc;
            
        % need to create temporary arrays for each field in adj_set so that
        % they can be passed to parfor loop 
        % (to be reassembled into a struct!)
            adj_list = cell(1,n_Realiz);
            rel_list = cell(1, n_Realiz);
            alpha_in = zeros(1,n_Realiz);
            alpha_out = zeros(1,n_Realiz);
            x_min = zeros(1,n_Realiz);
            L = zeros(1,n_Realiz);
            d_max = zeros(1,n_Realiz);
        
            for i = 1:n_Realiz
                adj_list{i} = adj_set(i).adj;
                rel_list{i} = adj_set(i).rel_list;
                alpha_in(i) = adj_set(i).alpha_in;
                alpha_out(i) = adj_set(i).alpha_out;
                d_max(i) = adj_set(i).d_max;
                L(i) = adj_set(i).L;
                x_min(i) = adj_set(i).x_min;
            end
               
            fprintf(['Average network parameters:\n',...
                '\t<alpha_out>: ',num2str(mean(alpha_out)),...
                '\n\td_max: \t\t',num2str(mean(d_max)),'\n'])
        %----------------------

        % open parallel worker pool
            %if matlabpool('size') == 0; matlabpool open; end % for Matlab 2013
            pl = gcp;
            
        % parallel loop (independent realisations)
            fprintf(['\n\nRunning ',num2str(n_Realiz),' simulations...\n']);
            tic;
            parfor i = 1:n_Realiz

                % create pre-generated network structure from existing data
                adj_data = struct('adj',adj_list{i},...
                                  'rel_list',rel_list{i},...
                                  'alpha_in',alpha_in(i),...
                                  'alpha_out',alpha_out(i),...
                                  'd_max',d_max(i),...
                                  'L',L(i),...
                                  'x_min',x_min(i))

                % init model (this time with VERBOSE mode off)
                    ibm = AMR_IBM(N, params, adj_data, VERBOSE, LOW_MEM);
                    ibm.DIR_NAME = DIR_NAME;
                    ibm.DEBUG = DEBUG;

                % specify parameter updates (if any)
                    ibm.param_updates = param_updates;

                % simulate
                    ibm.simulate(n_Days);

                % store data for this simulation realization
                    av_prevalence(:,:,i) = ibm.counters.prevalence;
                    av_prev_both(:,:,i) = ibm.counters.prev_both;
                    av_prev_either(:,:,i) = ibm.counters.prev_either;
                    av_prev_nonAMRonly(:,:,i) = ibm.counters.prev_nonAMRonly;
                    av_incidence(:,:,i) =  ibm.counters.incidence;
                    av_incidence_either(:,:,i) = ibm.counters.incidence_either;
                    av_incidence_new(:,:,i) = ibm.counters.incidence_new;
                    av_cefta(:,:,i) = ibm.counters.cefta;
                    av_cefta_notinf(:,:,i) = ibm.counters.cefta_notinf;
                    av_cefta_nonAMR(:,:,i) = ibm.counters.cefta_nonAMR;
                    av_cefta_AMR(:,:,i) = ibm.counters.cefta_AMR;
                    av_cipr(:,:,i) = ibm.counters.cipr;
                    av_cipr_notinf(:,:,i) = ibm.counters.cipr_notinf;
                    av_cipr_nonAMR(:,:,i) = ibm.counters.cipr_nonAMR;
                    av_cipr_AMR(:,:,i) = ibm.counters.cipr_AMR;
                    av_treatpositive(:,:,i) = ibm.counters.cipr-ibm.counters.cipr_notinf+ibm.counters.cefta-ibm.counters.cefta_notinf;
                    av_n_attended_seeked(:,:,i) = ibm.counters.n_attended_seeked;
                    av_n_attended_recalled(:,:,i) = ibm.counters.n_attended_recalled;
                    av_n_attended_traced(:,:,i) = ibm.counters.n_attended_traced;
                    av_n_attended_treated(:,:,i) = ibm.counters.n_attended_treated;
                    av_n_treated_infected_symptomatic(:,:,i) = ibm.counters.n_treated_infected_symptomatic;
                    av_n_screened(:,:,i) = ibm.counters.n_screened;
                    av_n_traced(:,:,i) = ibm.counters.n_traced;
                    av_int_dseq_all(:,:,i) = histc(full(sum(ibm.adj_int,2)),bin_edges);
                    av_int_dseq_ofinfected(:,:,i) = histc( full(sum( ibm.adj_int( any(ibm.indiv_state(:,:,end),2),:), 2)), bin_edges);

                % output status (note the completion order is not necessarily in seq.)
                    display(['    Simulation #',num2str(i),' complete!']);
            end
            toc;

            % Create plot compatible data structure from averaged values
            % (similar to .counters in the model class)
                data = struct('N',N,...
                            'prevalence',mean(av_prevalence,3),...
                            'prev_both',mean(av_prev_both,3),...
                            'prev_either',mean(av_prev_either,3),...
                            'prev_nonAMRonly',mean(av_prev_nonAMRonly,3),...
                            'prevalence_std',std(av_prevalence,0,3),...
                            'prev_both_std',std(av_prev_both,0,3),...
                            'prev_either_std',std(av_prev_either,0,3),...
                            'prev_nonAMRonly_std',std(av_prev_nonAMRonly,0,3),...
                            'incidence',mean(av_incidence,3),...
                            'incidence_either',mean(av_incidence_either,3),...
                            'incidence_new',mean(av_incidence_new,3),...
                            'cefta',mean(av_cefta,3),...
                            'cefta_notinf',mean(av_cefta_notinf,3),...
                            'cefta_nonAMR',mean(av_cefta_nonAMR,3),...
                            'cefta_AMR',mean(av_cefta_AMR,3),...
                            'cipr',mean(av_cipr,3),...
                            'cipr_notinf',mean(av_cipr_notinf,3),...
                            'cipr_nonAMR',mean(av_cipr_nonAMR,3),...
                            'cipr_AMR',mean(av_cipr_AMR,3),...
                            'treat_positive',mean(av_treatpositive,3),...
                            'n_attended_seeked', mean(av_n_attended_seeked,3),...
                            'n_attended_recalled', mean(av_n_attended_recalled,3),...
                            'n_attended_traced', mean(av_n_attended_traced,3),...
                            'n_attended_treated', mean(av_n_attended_treated,3),...
                            'n_treated_infected_symptomatic', mean(av_n_treated_infected_symptomatic,3),...
                            'n_screened', mean(av_n_screened,3),...
                            'n_traced', mean(av_n_traced,3),...
                            'int_dseq_all', mean(av_int_dseq_all,3),...
                            'int_dseq_ofinfected', mean(av_int_dseq_ofinfected,3)...
                            );
    end
 
    %% PLOTS (using class function code)
    
        set(0,'DefaultFigureRenderer','OpenGL');

        ave = 7; % average dosage data over weekly (7 day) intervals

        % don't save plots
            DIR_NAME = [];
            
        if SHOW_PLOTS
            [~,plot_names] = AMR_IBM.plot_prev(data, [0 n_Days], DIR_NAME);
            [h_fig,plot_names] = AMR_IBM.plot_incidence(data, [0 n_Days], ave, DIR_NAME);
            [h_fig_dose,plot_names] = AMR_IBM.plot_doses(data, ave, DIR_NAME);    
            [h_fig,plot_names] = AMR_IBM.plot_attendance(data, [0 n_Days], ave, DIR_NAME);
            [h_fig, plot_names] = AMR_IBM.plot_AMR_ratio(data, DIR_NAME);
            [h_fig, plot_names] = AMR_IBM.plot_diagnoses(data, DIR_NAME);
        end

    % SAVE DATA
    save(['./outdata/',DIR_NAME,'/plot_data.mat'],'data','N','n_Days','DIR_NAME','params');
   