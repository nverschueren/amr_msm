              %% generate node degree distribution

              clear; close all; clc;
                % min node degree
                d_min = 1;
                alpha=1.6; 
                d_max=120;
                N=5e3;
 
                % power-law distributed sequence between upper and lower bounds
                % (see matlab)
                node_degree = ( ( d_max^(-alpha+1) - d_min^(-alpha+1) ).*rand(N,1) + d_min^(-alpha+1) ).^( 1/(-alpha+1) );

                % get integer node degree sequence (how accurate is rounding here?)
                % should truncation be avoided instead rounding to nearest
                % int?
                tic;
                int_node_degree = fix(node_degree);
                
                % network constructor
   
                % init. list of free stubs for each node
                free_stubs = int_node_degree;

                % init edge list
                edge_list = [];
                rel_list = [];
                
                % sparse adjacency matrix
                adj = logical(sparse(N,N));

                % lookup vectors for each node's connections - too avoid self-loop and duplicates
                edge_lookup = num2cell((1:N)');

                % sequential loop through initial nodes
                % Does this need to be randomised? Since degree sequence is itself random, 
                % I *doubt* it makes any difference
                for this_node = randperm(N);%1:N   

                    % number of remaining stubs for selected node
                    my_stubs = free_stubs(this_node);

                    % free stubs available - excluding self and previously connected nodes
                    other_stubs = free_stubs;
                    other_stubs(edge_lookup{this_node}) = 0;

                    % loop over this node's free stubs
                    while my_stubs > 0
                        % indices of (other) nodes with free stubs
                        idx_avail_stubs = find(other_stubs > 0);

                        % if there is an available stub 
                        if ~isempty(idx_avail_stubs)
                            % chose one at random
                            that_node = idx_avail_stubs(randperm(length(idx_avail_stubs),1));
                            
                            % add to edge list (both directions)
                            edge_list = [edge_list;[this_node that_node];[that_node this_node]];
                            
                            % add to relationship list (one way)
                            rel_list = [rel_list;[this_node that_node]];
                            
                            % add to adjacency matrix
                            adj(this_node,that_node) = 1;
                            adj(that_node,this_node) = 1;

                            % reduce stub count for this node within this loop
                            my_stubs = my_stubs-1;

                            % reduce stub count for this node and connected node globally
                            free_stubs([this_node,that_node]) = free_stubs([this_node,that_node])-1;

                            % update lookup list - avoid duplicate edges
                            edge_lookup{this_node} = [edge_lookup{this_node} that_node];
                            edge_lookup{that_node} = [edge_lookup{that_node} this_node];

                            % update available stubs to this node
                            % free stubs available -  excluding this node and recently connected
                            other_stubs = free_stubs;
                            other_stubs(edge_lookup{this_node}) = 0;

                        else
                            % cannot find any free nodes for remaining stubs - break from this node
                            break
                        end

                    end % loop over node stubs


                end  % loop over selected node

                % add column of ones to edge list
                edge_list = [edge_list,ones(length(edge_list),1)];
                
                toc
                
                