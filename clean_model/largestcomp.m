%% Simplified script to initialise, run and plot model output
% (single model run)

    clear all;
    clc;
    close all;

    % General parmeters 
        N = 3e4/100;
        N% population size
        VERBOSE = true;
        LOW_MEM = false;
        load('base_params.mat','params');
    %% initialise model (create new model object)
        gono_model = AMR_IBM(N, params, [], VERBOSE, LOW_MEM);
     
        %extract the largest connected component
        g=graph(gono_model.adj_full); cc=conncomp(g); indices=find(cc==mode(cc));
        %Now, we reduce the size to just the lcc
        lcomp=gono_model.adj_full(indices,indices);x