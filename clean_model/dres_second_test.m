%Exploring the evolution of strains
clear;clc;
% General parmeters 
N = 1e3;          % population size
n_Days =  3*365;     % days to simulate
VERBOSE = false;
LOW_MEM = true;
% load preset simulation parameters from external file
% TEST 1
% Idea: At the beginning, for a population of N=1e3, infect 100% with non-AMR and 0% AMR. Also,
%set: the first 100 with seek_treatment_on=1 (the first day)
%     the second 100 with recall_notify=1
%      the third 100 with trace_notify=1
% Hence, the first 900 individuals will be in to_treat_today
% We expect that out of the 900  receiving treatment:
%    1) 900*dres_rate will turn dissapear from non-AMR and turn into AMR 
%    2) 900*(1-dres_rate) will be cured
% Hence sum(current_state)=[N 0]
% sum(new_state)=[100 900*dres_rate]
% cipr.count=900

load('base_params.mat','params');  
params.p0=[1 0 0]; %100% non-AMR, 0%amr, 0% coinfection
params.ALLOW_DRES=true;
params.ALLOW_COINFECTION=false;% no coinfection will be considered
params.dres_rate=0.2; %brutal, just for testing
params.ALLOW_TREAT=true;
params.P_BLINDTREAT_AS_AMR=0;
VERBOSE=false;
params.MU=0;
params.R=0;
params.BETA=0;
gono_model = AMR_IBM(N, params, [], VERBOSE, LOW_MEM);
%This is the day=0. 
sum(gono_model.indiv_state)
%Now we set the flags for treatment
[sum(isnan(gono_model.seeks_treatment_on)) sum(isnan(gono_model.recall_notify(:,2))) sum(isnan(gono_model.trace_notify(:,2)))]
gono_model.seeks_treatment_on(1:300)=1; %treat will be performed on day=1
gono_model.recall_notify(301:600,2)=1;
gono_model.trace_notify(601:900,2)=1;
[sum(isnan(gono_model.seeks_treatment_on)) sum(isnan(gono_model.recall_notify(:,2))) sum(isnan(gono_model.trace_notify(:,2)))]
disp('prediction')
disp('before')
[1000 0]
disp('after')
[100 900*gono_model.dres_rate]
disp('drug count')
900

before=gono_model.indiv_state;
gono_model.simulate(1);
after=gono_model.indiv_state;
disp('result')
disp('before')
sum(before)
disp('after')
sum(after)
disp('drug count')
[gono_model.counters.cipr(end) gono_model.counters.cefta(end)]







            
                   
            
