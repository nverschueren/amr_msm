AMR_IBM.m              : main model code (class)

base_params.mat     : data file with preset model parameters (in struct called 'params')

networks_N=1000...
and networks_N=10000...(.mat).  : these are data files with pre-generated sexual networks for populations of N = 1000, and N = 10000     respectively

shadedErrorBar.m      :  a third-party function for plots with shaded error bar regions


subaxis.m                    : a third-party function to organise subplots better on a single figure (can be mostly subsituted for 'subplot' if required)

parseArgs.m                : used by subaxis (above)


run_AMR_pregen.m        : a script to run the model, either for single runs, or multiple runs in parallel

run_simple.m            : 'simple' script to initialise, run and modify parameters of a single model run and some plots (play around with this first!).
